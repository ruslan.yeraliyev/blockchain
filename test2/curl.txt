root@test:/opt/eth# curl --location --request POST 'localhost:8545' \
> --header 'Content-Type: application/json' \
> --data-raw '{
>     "jsonrpc": "2.0",
>     "id": 3,
>     "method": "eth_accounts",
>     "params": []
> }'
{"jsonrpc":"2.0","id":3,"result":["0x7fe84bac8df6609181cf23a03d36095b0e6437fe"]}
root@test:/opt/eth# curl --location --request POST 'http://localhost:8545' \
> --header 'Content-type: application/json' \
> --data-raw '{
>     "jsonrpc": "2.0",
>     "id": 5,
>     "method": "personal_newAccount",
>     "params": [
>         "blockchain"
>     ]
> }'
{"jsonrpc":"2.0","id":5,"result":"0x19c44103a382b32db6aa1c69939c432b4acc96f0"}
root@test:/opt/eth# curl --location --request POST 'localhost:8545' --header 'Content-Type: application/json' --data-raw '{
    "jsonrpc": "2.0",
    "id": 3,
    "method": "eth_accounts",
    "params": []
}'
{"jsonrpc":"2.0","id":3,"result":["0x7fe84bac8df6609181cf23a03d36095b0e6437fe","0x19c44103a382b32db6aa1c69939c432b4acc96f0"]}

